package com.bfh.example;

import java.util.Iterator;

/**
 * Created by marc on 29.09.15.
 */
public class MyTree<E> implements Tree<E> {


    // auxilliary class for the tree nodes
    class TNode implements Position<E> {
        E elem;
        TNode parent;
        MyLinkedList<TNode> children = new MyLinkedList<>();
        Position<TNode> childrenPos;
        Object creator = MyTree.this;

        @Override
        public E element() {
            return elem;
        }
    }

    // instance variables
    private TNode root;
    private int size;

    @Override
    public Position<E> root() {
        return root;
    }

    @Override
    public Position<E> createRoot(E o) {
        if (o != null) {
            root = new TNode();
            root.elem = o;
            return root;
        }
        return null;
    }

    private TNode castToTNode(Position p){
        TNode n;
        try {
            n = (TNode) p;
        } catch (ClassCastException e) {
            throw new RuntimeException("This is not a com.bfh.example.Position belonging to com.bfh.example.MyTree");
        }
        if (n.creator == null) throw new RuntimeException("position was allready deleted!");
        if (n.creator != this) throw new RuntimeException("position belongs to another com.bfh.example.MyTree instance!");
        return n;
    }

    @Override
    public Position<E> parent(Position<E> child) {
        TNode n = castToTNode(child);
        return n.parent;
    }

    @Override
    public Iterator<Position<E>> childrenPositions(Position<E> parent) {
        final TNode n = castToTNode(parent);
        return new Iterator<Position<E>>() {
            Iterator<TNode> it = n.children.elements();
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public Position<E> next() {
                return it.next();
            }
        };
        //return null;
    }

    @Override
    public Iterator<E> childrenElements(Position<E> parent) {
        return null;
    }

    @Override
    public int numberOfChildren(Position<E> parent) {
        TNode n = castToTNode(parent);
        return n.children.size();
    }

    @Override
    public Position<E> insertParent(Position<E> p, E o) {
        return null;
    }

    @Override
    public Position<E> addChild(Position<E> parent, E o) {
        TNode n = castToTNode(parent);
        TNode newN = new TNode();
        newN.elem = o;
        newN.parent = n;
        newN.childrenPos = n.children.insertLast(newN);
        size++;

        return newN;
    }

    @Override
    public Position<E> addChildAt(int pos, Position<E> parent, E o) {
        return null;
    }

    @Override
    public Position<E> addSiblingAfter(Position<E> sibling, E o) {
        return null;
    }

    @Override
    public Position<E> addSiblingBefore(Position<E> sibling, E o) {
        return null;
    }


    @Override
    public void remove(Position<E> p) {
        TNode n = castToTNode(p);
        if (n.children.size() != 0) throw new RuntimeException("cannot remove node with children!");
        n.creator = null;
        size--;
        if (n == root) root = null;
        else n.parent.children.remove(n.childrenPos);
    }

    public void removeSubtree(Position<E> p) {
        Iterator<Position<E>> it = childrenPositions(p);
        while (it.hasNext()) removeSubtree(it.next());
        remove(p);
    }

    @Override
    public boolean isExternal(Position<E> p) {
        return false;
    }

    @Override
    public boolean isInternal(Position<E> p) {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public E replaceElement(Position<E> p, E o) {
        return null;
    }

    private void print(Position<E> r, String in) {
        System.out.println(in + r.element().toString());
        Iterator<Position<E>> it = childrenPositions(r);
        while (it.hasNext()) {
            print(it.next(), "..." + in);
        }
    }

    private int treeHeight(Position<E> r){
        int max=0;
        Iterator<Position<E>> it = childrenPositions(r);
        while (it.hasNext()){
            int height = treeHeight(it.next());
            if(height > max) {
                max = height;
            }
        }
        return ++max;

    }

    public static void main(String[] args) {
        MyTree<String> t = new MyTree<String>();
        Position<String> tit = t.createRoot("Buch");
        Position<String> k1 = t.addChild(tit, "Kapitel 1");
        Position<String> k2 = t.addChild(tit, "Kapitel 2");
        Position<String> k3 = t.addChild(tit, "Kapitel 3");
        Position<String> k4 = t.addChild(tit, "Kapitel 4");
        Position<String> title1 = t.addChild(k1, "Intro");
        Position<String> title2 = t.addChild(k1, "Die Welt");
        //Position<String> title4 = t.addChild(k1, "Der Mars");
        Position<String> title3 = t.addChild(k2, "Das Haus");
        t.print(tit,"");
        System.out.println(t.treeHeight(tit));
    }
}
